﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[SelectionBase] //выбирает первый объект в группе при выделении на сцене
public class GridCell : MonoBehaviour
{

    public bool isExplored = false;
    public GridCell exploredFrom; 
    public int lengthPath;


    public bool isEmpty;

    SpriteRenderer renderer;
    Collider collider;

    public Color defaultColor; //= Color.cyan;
    public Color highlightedColor; //= Color.white;
    public Color activeCharColor; //= Color.red;


    private void Awake()
    {
        isEmpty = true;
        renderer = GetComponentInChildren<SpriteRenderer>();
        collider = GetComponent<Collider>();
        defaultColor = renderer.color;

    }

    public Vector2Int GetCellIndex()
    {
        Vector2Int index = new Vector2Int(
            (int)transform.position.x,
            (int)transform.position.z
            );
        return index;

    }

    public void SetHighlitedColor()
    {

        renderer.color = highlightedColor;
    }

    public void ResetColor()
    {
        renderer.color = defaultColor;
    }

    public void SetActiveCharColor()
    {
        collider.enabled = false;
        renderer.color = activeCharColor;
    }



    //private void OnMouseEnter()
    //{
    //    SetColor(Color.white);
    //}
    //private void OnMouseExit()
    //{
    //    SetColor(Color.grey);
    //}

    public void ResetCell()
    {
        collider.enabled = true;
        isExplored = false;
        exploredFrom = null;
        lengthPath = 0;
        ResetColor();
    }
}
