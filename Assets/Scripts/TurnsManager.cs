﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


public class TurnsManager : MonoBehaviour
{

    public Action OnNextTurn = delegate { }; //delegate позволяет экшену выполняться даже с значением null 


    [SerializeField] List<CharacterController> playerCharacters;
    [SerializeField] List<CharacterController> enemyCharacters;

    PathFinding pathfinding;
    bool allowInput;

    [Header("Cursors")]
    [SerializeField] Texture2D cursorNormal;
    [SerializeField] Texture2D cursorDisabled;
    [SerializeField] Texture2D cursorAttack;
    [SerializeField] Texture2D cursorMove;
    
    CharacterController currentCharacter;
    List<CharacterController> charactersQueue;
   
    public List<CharacterController> CharacterQueue //делаем паблик из списка charactersQueue
    {
        get
        {
            return charactersQueue;
        }
    }

   

    void Start()
    {
        SetCursor(cursorNormal);
       
        Pathfinding();
        foreach(CharacterController element in charactersQueue)
        {
            element.OnCharacterDeath += CharacterDied;
        }
    }


    void SetCursor(Texture2D cursor)
    {
        Cursor.SetCursor(cursor, Vector2.zero, CursorMode.Auto);
    }


    void CharacterDied(CharacterController character)
    {
        charactersQueue.Remove(character);
        playerCharacters.Remove(character);
        enemyCharacters.Remove(character);

        if (playerCharacters.Count == 0)
        {
            Debug.Log("Defeat!");
        }

        if (enemyCharacters.Count == 0)
        {
            Debug.Log("Victory!");
        }

    }


    void Pathfinding()
    {
        pathfinding = FindObjectOfType<PathFinding>();
        //CharacterController[] objects = FindObjectsOfType<CharacterController>();
        charactersQueue = new List<CharacterController>();
        charactersQueue.AddRange(playerCharacters);
        charactersQueue.AddRange(enemyCharacters);
        //currentCharacter = characters[0];
        //pathfinding.ScanGrid(currentCharacter);

        NextTurn();
        allowInput = true;
    }


    void NextTurn()
    {
        int currentIndex = charactersQueue.IndexOf(currentCharacter);
        int newIndex = currentIndex + 1;
        if (newIndex >= charactersQueue.Count)
        {
            newIndex = 0;
            //new round
        }
        currentCharacter = charactersQueue[newIndex];
        currentCharacter.AnimationSetIdle();
        pathfinding.ScanGrid(currentCharacter);

        OnNextTurn();


    }

    GridCell lastCellReycast;



    void Update()

    {


        if (allowInput)
        {
            if (lastCellReycast != null)
            {
                lastCellReycast.ResetColor();
            }
            SetCursor(cursorNormal);
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 100.0f))
            {
                GridCell cell = hit.transform.GetComponent<GridCell>();

                if (cell != null)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        currentCharacter.MoveTo(cell, FinishedMovement);
                        allowInput = false;

                    }
                    cell.SetHighlitedColor();
                    lastCellReycast = cell;
                }

                else
                {
                    CharacterController character = hit.transform.GetComponent<CharacterController>();
                    if (character != null && character != currentCharacter && !IsInOneTeam(currentCharacter, character)) 
                        //навели на персонажа, персонаж не текущий персонаж, в одной команде: нет 
                    {
                       
                        if (Input.GetMouseButtonDown(0))

                        {
                            if (character.CurrentPosition.lengthPath <= currentCharacter.Speed)
                            {
                                //Debug.Log("hit!");
                                
                                allowInput = false;
                                currentCharacter.Attack(character, FinishedMovement);
                                //character.GetDamage();
                                SetCursor(cursorNormal);
                            }

                        }

                    }

                }
            }
        }


    }

    bool IsInOneTeam(CharacterController character1, CharacterController character2)
    {
        bool isInOneTeam = playerCharacters.Contains(character1) && playerCharacters.Contains(character2) 
                        || enemyCharacters.Contains(character1) && enemyCharacters.Contains(character2);

        return isInOneTeam;
        
    }

    void FinishedMovement()
    {
        NextTurn();
        allowInput = true;

    }




}
