﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[SelectionBase]  //выделять парент обьекта 
public class CharacterController : MonoBehaviour
{

    public Action<CharacterController> OnCharacterDeath;


    [SerializeField] int speed;
    [SerializeField] float moveSpeed;
    [SerializeField] Sprite portraitPrefab;        
    [SerializeField] int initiative;
    [SerializeField] int health;
    [SerializeField] int attackPower;
    [SerializeField] int actualHealth;


    PathFinding pathfinding;
    Animator animator;
    Action finishedAction;

    CharacterController attackTarget;

    public Sprite Portrait
    {
        get
        {          return  portraitPrefab;     }
    }
    public int Speed   //то же что и выше
    {
        get { return speed; }
        //set { speed = value; }
    }

    public GridCell CurrentPosition
    {
        get;
        private set;
    }

    void Start()
    {
        animator = GetComponentInChildren<Animator>();
        pathfinding = FindObjectOfType<PathFinding>();


        Vector2Int snappedPosition = new Vector2Int();
        snappedPosition.x = Mathf.RoundToInt(transform.position.x);
        snappedPosition.y = Mathf.RoundToInt(transform.position.z);

        CurrentPosition = pathfinding.GetCell(snappedPosition);
        transform.position = CurrentPosition.transform.position;
        CurrentPosition.isEmpty = false;

        actualHealth = health;





    }


    void Update()
    {

    }

    public void MoveTo(GridCell cell, Action finishedMovement)
    {

        List<GridCell> path = pathfinding.GetPath(cell);

        CurrentPosition.isEmpty = true;
        CurrentPosition = cell;
        cell.isEmpty = false;

        //transform.position = currentPosition.transform.position;
        StartCoroutine(MoveInPath(path, finishedMovement));


    }


    IEnumerator MoveInPath(List<GridCell> waypoints, Action finishedMovement)

    {

        animator.SetTrigger("Move");
        // foreach (GridCell element in waypoints)
        for (int i = 0; i < waypoints.Count; i++)
        {

            GridCell element = waypoints[i];
            Vector3 target = element.transform.position;

            while (Vector3.Distance(transform.position, element.transform.position) > 0.005f)
            {
                transform.position = Vector3.MoveTowards(transform.position, target, moveSpeed * Time.deltaTime);
                yield return null;
            }




            int nextElementIndex = i + 1;
            if (nextElementIndex < waypoints.Count)
            {
                GridCell nextElement = waypoints[i + 1];
                transform.LookAt(nextElement.transform.position);

            }
            yield return null;
        }
        animator.SetTrigger("Stand");
        finishedMovement();
    }

    public void AnimationSetIdle()
    {
        animator.SetTrigger("Idle");
    }

    public void GetDamage()
    {

        actualHealth -= attackPower;
        animator.SetTrigger("GetDamage");

        if (actualHealth <= 0)
        {
            Die();

            //Destroy(gameObject);
        }

        // Debug.Log("hit" + actualHealth);
    }

    void Die()
    {
        animator.SetTrigger("Death");
        OnCharacterDeath(this);
        Collider collider = GetComponent<Collider>();
        collider.enabled = false;
        CurrentPosition.isEmpty = true;
    }

    public void Attack(CharacterController target, Action finishedMovement)

    {
        attackTarget = target;
        finishedAction = finishedMovement;

        if (target.CurrentPosition.lengthPath <= speed)
        {
            if (target.CurrentPosition.lengthPath > 1)
            {
                MoveTo(target.CurrentPosition.exploredFrom, AttackTarget);
            }
            else
            {
                AttackTarget();

            }

        }
        //else
        //{
        //    MoveTo();
        //}
    }

    void AttackTarget()
    {
       

        transform.LookAt(attackTarget.transform.position);
        animator.SetTrigger("Attack");
        StartCoroutine(ActionWithDelay(finishedAction, 3));
    }

    public void HitMoment()
    {
        attackTarget.GetDamage();
        attackTarget = null;

    }

    IEnumerator ActionWithDelay(Action finishedAction, float delay)
    {
        yield return new WaitForSeconds(delay);
        finishedAction();


    }

}
