﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour
{
    [Header("Queue panel")]
    [SerializeField] GameObject queuePanel;
    [SerializeField] GameObject portraitPrefab;

    TurnsManager turnsManager;

    void Start()
    {
        turnsManager = FindObjectOfType<TurnsManager>();
        DestroyAllChildren();
        FillQueuePanel();

        //turnsManager.OnNextTurn += MethodA; //записать в экшен  MethodA()
        //turnsManager.OnNextTurn += MethodB; //записать в экшен  MethodB()
        turnsManager.OnNextTurn += OnNextTurn;
        ;

    }

    private void FillQueuePanel()
    {
        List<CharacterController> queue = turnsManager.CharacterQueue;

        foreach (CharacterController element in queue)
        {
            GameObject newPortrait = Instantiate(portraitPrefab, queuePanel.transform); //создать портрет, перенести его в панель
            newPortrait.GetComponent<Image>().sprite = element.Portrait; //забирем конкретную картинку из префаба персонажа
        }
    }

    //void MethodA()
    //{ print("Method A");

    //}
    //void MethodB()
    //{

    //    print("Method b");
    //}
    void OnNextTurn()
    {
        queuePanel.transform.GetChild(0).SetAsLastSibling(); //элемент 0 из очереди перенести в конец очереди

        //DestroyAllChildren();
        //FillQueuePanel();
    }

    private void DestroyAllChildren()
    {
        for (int i = queuePanel.transform.childCount - 1; i >= 0; i--)
        {
            Transform childI = queuePanel.transform.GetChild(i);
            Destroy(childI.gameObject);
        }
    }

    void Update()
    {
        
    }
}
