﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PathFinding : MonoBehaviour
{


    [SerializeField] GridCell startCell;
    [SerializeField] GridCell endCell;

    Dictionary<Vector2Int, GridCell> grid = new Dictionary<Vector2Int, GridCell>();
    Queue<GridCell> queue = new Queue<GridCell>();

    Vector2Int[] directions =
    {
        //new Vector2Int(0, 1),
        //new Vector2Int(1, 0),
        //new Vector2Int(0, -1),
        //new Vector2Int(-1, 0),

        Vector2Int.up,

        Vector2Int.right,

        Vector2Int.down,

        Vector2Int.left,
        new Vector2Int(1, 1),
        new Vector2Int(1, -1),

        new Vector2Int(-1, -1),
        new Vector2Int(-1, 1),

    };

    void Start()
    {
        LoadGrid();


    }

    public GridCell GetCell(Vector2Int coordinates)
    {
        return grid[coordinates];


    }

    public void ScanGrid(CharacterController character)
    {


        RefreshGrid();
        //character.AnimationSetIdle(); //перенесли в TurnsManager
        GridCell firstCell = character.CurrentPosition;
        queue.Enqueue(character.CurrentPosition);

        firstCell.SetActiveCharColor();
        while (queue.Count > 0)
        {

            GridCell element = queue.Dequeue(); //убрать из очереди
            if (element == firstCell || element.isEmpty)
            {
                ExploreNeighbours(element);  //исследовать соседние клетки
            }
            element.isExplored = true; //пометить как исследованный 

            if (element.lengthPath > character.Speed || (!element.isEmpty && element != firstCell))
            {

                element.gameObject.SetActive(false);

            }

        }

    }


    public List<GridCell> GetPath(GridCell endCell)
    {
        List<GridCell> path = new List<GridCell>(); //собираем массив cells explored from 

        GridCell nextCell = endCell; //начинаем с последней cell

        path.Add(endCell);
        while (nextCell.exploredFrom != null) //у стартовой точки nextCell равен null, алгоритм закончен
        {
            path.Add(nextCell.exploredFrom); //записываем в список path     
            nextCell.SetHighlitedColor();
            nextCell = nextCell.exploredFrom; //подменяем переменную для цикла


        }

        path.Reverse(); //записать путь в нужном направлении (со старта в финиш) 
        return path;
    }
    //public List<GridCell> FindPath(GridCell startCell, GridCell endCell)
    //{
    //    RefreshGrid();
    //    queue.Enqueue(startCell); //добавить в очередь

    //    while (queue.Count > 0) 
    //    {
    //        GridCell element = queue.Dequeue(); //убрать из очереди
    //        if (element == endCell)
    //        {
    //            //pathfound
    //            break; //остановить алгоритм
    //        }
    //        ExploreNeighbours(element);  //исследовать соседние клетки
    //        element.isExplored = true; //пометить как исследованный 
    //    }


    //    List<GridCell> path = new List<GridCell>(); //собираем массив cells explored from 

    //    GridCell nextCell = endCell; //начинаем с последней cell

    //    path.Add(endCell);
    //    while (nextCell.exploredFrom != null) //у стартовой точки nextCell равен null, алгоритм закончен
    //    {
    //        path.Add(nextCell.exploredFrom); //записываем в список path     
    //        nextCell.SetColor(Color.green);
    //        nextCell = nextCell.exploredFrom; //подменяем переменную для цикла


    //    }

    //    path.Reverse(); //записать путь в нужном направлении (со старта в финиш) 
    //    return path;
    //}


    private void RefreshGrid()
    {
        queue.Clear();
        foreach (GridCell element in grid.Values)
        {

            element.gameObject.SetActive(true);

            element.ResetCell();

        }


    }
    private void ExploreNeighbours(GridCell cell)
    {
        foreach (Vector2Int element in directions)
        {
            Vector2Int neighborCoordinates = cell.GetCellIndex() + element; //добавить к текущей клетке element из списка directions

            if (grid.ContainsKey(neighborCoordinates)) //в сетке grid есть элемент cell
            {
                GridCell neighbour = grid[neighborCoordinates];



                if (queue.Contains(neighbour) || neighbour.isExplored) //если уже в очереди или нет элемента ничего не делать   
                {
                    //do nothing

                }
                else //добавить в очередь
                {
                    queue.Enqueue(neighbour); //добавить в очередь
                    neighbour.exploredFrom = cell; //запомнить клетку, из которой попал в очередь

                    int length = 0;
                    GridCell nextCell = neighbour;
                    while (nextCell.exploredFrom != null) //у стартовой точки nextCell равен null, алгоритм закончен
                    {
                        length++;
                        nextCell = nextCell.exploredFrom; //подменяем переменную для цикла


                    }
                    neighbour.lengthPath = length;
                }

            }
        }
    }

    private void LoadGrid()
    {
        GridCell[] cells = FindObjectsOfType<GridCell>(); //var cells или GridCell[] cells - это всё массив
        foreach (GridCell element in cells)
        {
            grid.Add(element.GetCellIndex(), element);
        }
    }
}
