﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[DisallowMultipleComponent]
public class Grid : MonoBehaviour
{
    [SerializeField] int rows = 10;
    [SerializeField] int columns = 10;
    [SerializeField] GameObject cellPrefab;


    public void GenerateGrid()
    {
        ClearGrid();

        for (int column = 0; column < columns; column++)
        {
            for (int row = 0; row < rows; row++)
            {
                //print(column + "," + row);
                Vector3 position = new Vector3(row, 0, column);

                GameObject newCell = PrefabUtility.InstantiatePrefab(cellPrefab) as GameObject;
                //GameObject newCell = Instantiate(cellPrefab, position, Quaternion.identity); //Quaternion.identity - без поворота
                newCell.transform.position = position;
                newCell.transform.parent = transform; //располагать внутри парента
                //string text = row + "," + column;
                //newCell.name = text; //переименовать NewCell                                
                //newCell.GetComponentInChildren<TextMesh>().text = text; //set text on each cell

            }
        }

    }

    private void ClearGrid()
    {
        for (int childIndex = transform.childCount - 1; childIndex >= 0; childIndex--)
        {
            DestroyImmediate(transform.GetChild(childIndex).gameObject);
            //удалить всех чайлдов с последнего к первому
        }
    }


}
